package net.tyan.battleroyal;

import net.tyan.battleroyal.achievement.AchievementHandler;
import net.tyan.battleroyal.achievement.data.AchievementDataHandler;
import net.tyan.battleroyal.achievement.data.MySQLAchievementDataManager;
import net.tyan.battleroyal.commands.CheatCommand;
import net.tyan.battleroyal.commands.StatsCommand;
import net.tyan.battleroyal.data.MySQL;
import net.tyan.battleroyal.data.MySQLHandler;
import net.tyan.battleroyal.game.GameState;
import net.tyan.battleroyal.listener.LobbyListener;
import net.tyan.battleroyal.listener.PlayerJoinListener;
import net.tyan.battleroyal.listener.PlayerQuitListener;
import net.tyan.battleroyal.scheduler.LobbyScheduler;
import net.tyan.battleroyal.user.User;
import net.tyan.battleroyal.utility.ListenerBundle;
import net.tyan.battleroyal.utility.RegisterUtil;
import net.tyan.battleroyal.utility.WorldHandler;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * by Kevin on 23.07.2015.
 */

public class BattleRoyal extends JavaPlugin {

    private static BattleRoyal instance;
    private MySQL mySQL;
    private MySQLHandler handler;

    public Map<UUID, User> activeUsers;
    private GameState gameState;

    private AchievementHandler achievementHandler;
    private AchievementDataHandler achievementDataHandler;

    private RegisterUtil<BattleRoyal> registerUtil;

    @Override public void onLoad() {
        instance = this;
    }

    @Override public void onEnable() {
        mySQL = new MySQL();
        handler = new MySQLHandler();
        activeUsers = new HashMap<>();
        gameState = GameState.LOBBY;

        /* Table creation if not exists */
        handler.new Query(
            "CREATE TABLE IF NOT EXISTS users (UUID char(32), Kills int, Deaths int, Points int, PlayedRounds int, RoundsWon int, ChestOpened int, Achievements text);",
            false);

        achievementHandler = new AchievementHandler();
        achievementDataHandler = new MySQLAchievementDataManager();

        registerUtil = new RegisterUtil<>(this);

        ListenerBundle basicListeners = new ListenerBundle();
        basicListeners.add(new PlayerJoinListener(), new PlayerQuitListener());
        ListenerBundle.register("basics", basicListeners);

        ListenerBundle lobbyListener = new ListenerBundle();
        lobbyListener.add(new LobbyListener());
        ListenerBundle.register("lobby", lobbyListener);

        registerUtil.registerCommand("stats", "Shows your stats", new StatsCommand(), "");
        registerUtil.registerCommand("cheat", "Cheat command", new CheatCommand(),
            "battleroyal.admin");

        WorldHandler.loadWorld("battleroyal");

        new LobbyScheduler(this);
    }

    @Override public void onDisable() {
        try {
            mySQL.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        WorldHandler.unloadWorld("battleroyal");
        WorldHandler.removeWorld(new File("battleroyal"));

    }

    public static BattleRoyal getInstance() {
        return instance;
    }

    public MySQL getMySQL() {
        return mySQL;
    }

    public MySQLHandler getMySQLHandler() {
        return handler;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public AchievementHandler getAchievementHandler() {
        return achievementHandler;
    }

    public AchievementDataHandler getAchievementDataHandler() {
        return achievementDataHandler;
    }

    public RegisterUtil<BattleRoyal> getRegisterUtil() {
        return registerUtil;
    }
}

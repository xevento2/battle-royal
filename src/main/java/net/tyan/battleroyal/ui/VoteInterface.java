package net.tyan.battleroyal.ui;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

/**
 * by Kevin on 24.07.2015.
 */

public class VoteInterface  {

    private final Inventory inventory;
    private final Player player;

    public VoteInterface(Player player) {
        this.player = player;
        this.inventory = Bukkit.createInventory(player, InventoryType.ANVIL, "§6Vote");
    }
}

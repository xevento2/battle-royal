package net.tyan.battleroyal.ui;

import net.tyan.battleroyal.BattleRoyal;
import net.tyan.battleroyal.achievement.Achievement;
import net.tyan.battleroyal.achievement.AchievementHandler;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * by Kevin on 24.07.2015.
 */

public class AchievementInterface {

    private final Inventory inventory;
    private final Player player;

    public AchievementInterface(Player player) {
        this.inventory = Bukkit.createInventory(player, InventoryType.CHEST, "§6Achievements");
        this.player = player;

        setItems();
    }

    private void setItems() {
        int counter = 0;
        for (AchievementHandler.Achievements achievement : AchievementHandler.Achievements.values()) {
            boolean enchant = false;
            if (BattleRoyal.getInstance().getAchievementDataHandler().getReachedAchievements(player.getUniqueId()).contains(achievement.getAchievement().getId()))
                enchant = true;
            inventory.setItem(counter, getItem(achievement.getAchievement(), enchant));
            counter++;
        }
        open();
    }


    private ItemStack getItem(Achievement achievement, boolean enchant) {
        ItemStack itemStack = new ItemStack(Material.PAPER);
        ItemMeta itemMeta = itemStack.getItemMeta();
        String displayName = "§6§k" + achievement.getName();
        if (enchant) {
            itemMeta.addEnchant(Enchantment.DAMAGE_ALL, 1, true);
            displayName = "§6" + achievement.getName();
        }
        itemMeta.setDisplayName(displayName);
        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }

    private void open() {
        player.openInventory(inventory);
    }

}

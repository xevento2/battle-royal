package net.tyan.battleroyal.listener;

import net.tyan.battleroyal.BattleRoyal;
import net.tyan.battleroyal.game.GameState;
import net.tyan.battleroyal.scheduler.PreparingScheduler;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * by Kevin on 24.07.2015.
 */

public class PreparingListener implements Listener {

    public static List<Player> ignored;

    public PreparingListener() {
        this.ignored = new ArrayList<>();
    }

    @EventHandler public void onDamage(EntityDamageEvent event) {
        event.setCancelled(true);
    }

    @EventHandler public void onDamageByEntity(EntityDamageByEntityEvent event) {
        event.setCancelled(true);
    }

    @EventHandler public void onMove(PlayerMoveEvent event) {
        if (ignored.contains(event.getPlayer()))
            return;
        if (event.getPlayer().isSneaking() || event.getPlayer().getLocation().getBlock().getRelative(BlockFace.DOWN).getType().isSolid()) {
            ignored.add(event.getPlayer());
        }

        Player p = event.getPlayer();
        p.setVelocity(
            new Vector(p.getLocation().getDirection().getX() * 0.8, p.getVelocity().getY() * 0.8,
                p.getLocation().getDirection().getZ() * 0.3));
        p.setFallDistance(0.0F);
    }
}

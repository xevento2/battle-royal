package net.tyan.battleroyal.listener;

import net.tyan.battleroyal.BattleRoyal;
import net.tyan.battleroyal.reference.Reference;
import net.tyan.battleroyal.user.User;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 * by Kevin on 24.07.2015.
 */

public class InGameListener implements Listener {

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        event.setDeathMessage(null);
        if (Bukkit.getOnlinePlayers().size() == 1) {
            Bukkit.broadcastMessage(Reference.PREFIX + "§7" + event.getEntity().getName() + " §6hat das Spiel gewonnen!");
            for (Player player : Bukkit.getOnlinePlayers()) {
                BattleRoyal.getInstance().activeUsers.get(player.getUniqueId()).update();
            }
            Bukkit.shutdown();
        }
        Bukkit.broadcastMessage(Reference.PREFIX + "§7" + event.getEntity().getName() + " §6 wurde von §7" + event.getEntity().getKiller().getName() + " §6get\u00f6tet");
        event.getEntity().kickPlayer("§6Du wurdest von §7" + event.getEntity().getKiller().getName() + "§6 get\u00f6tet!");

    }

}

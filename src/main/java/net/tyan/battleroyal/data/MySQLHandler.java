package net.tyan.battleroyal.data;

import net.tyan.battleroyal.BattleRoyal;
import net.tyan.battleroyal.user.User;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitScheduler;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * by Kevin on 23.07.2015.
 */

public class MySQLHandler {

    private final BukkitScheduler scheduler;
    private final MySQL mySQL;

    public MySQLHandler() {
        this.scheduler = Bukkit.getScheduler();
        mySQL = BattleRoyal.getInstance().getMySQL();
    }

    public abstract class SQLAction implements Runnable, Closeable {
        protected Connection connection = null;
        protected PreparedStatement statement = null;
        protected ResultSet resultSet = null;

        protected SQLAction() {
            boolean ASYNC = true;
            if (ASYNC) {
                scheduler.runTaskAsynchronously(BattleRoyal.getInstance(), this);
            } else {
                run();
            }
        }

        @Override
        public final void close() {
            try {
                if (connection != null)
                    connection.close();
                if (statement != null)
                    statement.close();
                if (resultSet != null)
                    resultSet.close();
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public class Query extends SQLAction {

        private final String query;
        private final Object[] parameters;
        private final boolean parametersAvailable;

        public Query(String query, boolean parametersAvailable, Object... parameters) {
            super();
            this.query = query;
            this.parametersAvailable = parametersAvailable;
            this.parameters = new Object[parameters.length];

            if (parametersAvailable) {
                System.arraycopy(parameters, 0, this.parameters, 0, parameters.length);
            }
        }

        @Override
        public void run() {
            try {
                connection = mySQL.getConnection();
                statement = connection.prepareStatement(query);
                if (parametersAvailable) {
                    for (int i = 0; i < parameters.length; i++) {
                        statement.setObject(i + 1, parameters[i]);
                    }
                }
                statement.executeUpdate();
            } catch (final Exception e) {
                e.printStackTrace();
            } finally {
                close();
            }
        }
    }

    public class LoadUser extends SQLAction {

        private final String uuid;
        private final String compactUUID;

        public LoadUser(String uuid) {
            super();
            this.uuid = uuid;
            compactUUID = uuid.replaceAll("-", "");
        }

        @Override
        public void run() {
            try {
                connection = mySQL.getConnection();
                statement = connection.prepareStatement("SELECT * FROM users WHERE UUID = ?;");
                statement.setString(1, compactUUID);

                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    User user = BattleRoyal.getInstance().activeUsers.get(UUID.fromString(uuid));
                    user.setKills(resultSet.getInt("Kills"));
                    user.setDeaths(resultSet.getInt("Deaths"));
                    user.setPoints(resultSet.getInt("Points"));
                    user.setPlayedRounds(resultSet.getInt("PlayedRounds"));
                    user.setRoundsWon(resultSet.getInt("RoundsWon"));
                    user.setChestOpened(resultSet.getInt("ChestOpened"));
                    user.setAchievements(resultSet.getString("Achievements"));
                    BattleRoyal.getInstance().activeUsers.replace(user.getUUID(), user);
                }
            } catch (final Exception e) {
                e.printStackTrace();
            } finally {
                close();
            }
        }
    }
}

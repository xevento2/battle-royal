package net.tyan.battleroyal.data;

import com.zaxxer.hikari.HikariDataSource;
import net.tyan.battleroyal.reference.Reference;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * by Kevin on 23.07.2015.
 */

public class MySQL implements Closeable {

    private final HikariDataSource dataSource;

    public MySQL() {
        this.dataSource = new HikariDataSource();
        dataSource.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        dataSource.addDataSourceProperty("serverName", Reference.MYSQL_HOST);
        dataSource.addDataSourceProperty("port", Reference.MYSQL_PORT);
        dataSource.addDataSourceProperty("databaseName", Reference.MYSQL_DATABASE);
        dataSource.addDataSourceProperty("user", Reference.MYSQL_USER);
        dataSource.addDataSourceProperty("password", Reference.MYSQL_PASSWORD);

        dataSource.setPoolName("BattleRoyal pool");
        dataSource.setMinimumIdle(2);
        dataSource.addDataSourceProperty("useUnicode", "true");
        dataSource.addDataSourceProperty("characterEncoding", "utf-8");
        dataSource.addDataSourceProperty("rewriteBatchedStatements", "true");

        dataSource.addDataSourceProperty("cachePrepStmts", "true");
        dataSource.addDataSourceProperty("prepStmtCacheSize", "250");
        dataSource.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
    }


    @Override
    public void close() throws IOException {
        dataSource.close();
    }

    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}

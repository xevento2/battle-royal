package net.tyan.battleroyal.achievement;

import net.tyan.battleroyal.BattleRoyal;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

/**
 * by Kevin on 24.07.2015.
 */

public class BaseAchievement implements Achievement, Listener {

    private final String id;
    private final String name;

    public BaseAchievement(String id, String name) {
        this.id = id;
        this.name = name;
        Bukkit.getServer().getPluginManager().registerEvents(this, BattleRoyal.getInstance());
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void reach(Player player) {
        BattleRoyal.getInstance().getAchievementHandler().addReachedAchievement(player, this);
    }
}

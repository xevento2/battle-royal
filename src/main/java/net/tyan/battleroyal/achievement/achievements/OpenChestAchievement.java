package net.tyan.battleroyal.achievement.achievements;

import net.tyan.battleroyal.achievement.BaseAchievement;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * by Kevin on 24.07.2015.
 */

public class OpenChestAchievement extends BaseAchievement {


    public OpenChestAchievement() {
        super("0", "OpenChest");
    }

    @EventHandler public void onReach(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock().getType()
            .equals(Material.CHEST)) {
            reach(event.getPlayer());
        }
    }
}

package net.tyan.battleroyal.achievement.data;

import net.tyan.battleroyal.BattleRoyal;
import net.tyan.battleroyal.data.MySQLHandler;
import net.tyan.battleroyal.user.User;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * by Kevin on 24.07.2015.
 */

public class MySQLAchievementDataManager implements AchievementDataHandler {

    @Override
    public ArrayList<String> getReachedAchievements(UUID player) {
        ArrayList<String> reachedAchievements = new ArrayList<>();

        User user = BattleRoyal.getInstance().activeUsers.get(player);
        String achievements = user.getAchievements();

        Collections.addAll(reachedAchievements, achievements.split(";"));

        return reachedAchievements;
    }

    @Override
    public void setReachedAchievements(Player player, List<String> reachedAchievements) {
        StringBuilder builder = new StringBuilder();
        reachedAchievements.forEach(s -> builder.append(s).append(";"));
        String listAsString = builder.toString().substring(0, builder.length() - 1);
        User user = BattleRoyal.getInstance().activeUsers.get(player.getUniqueId());
        user.setAchievements(listAsString);
        user.update();
    }
}

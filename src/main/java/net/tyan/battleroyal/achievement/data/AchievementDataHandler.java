package net.tyan.battleroyal.achievement.data;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * by Kevin on 24.07.2015.
 */

public interface AchievementDataHandler {

    ArrayList<String> getReachedAchievements(UUID player);

    void setReachedAchievements(Player player, List<String> reachedAchievements);

}

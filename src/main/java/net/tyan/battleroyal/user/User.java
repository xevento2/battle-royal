package net.tyan.battleroyal.user;

import net.tyan.battleroyal.BattleRoyal;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * by Kevin on 23.07.2015.
 */

public class User {

    private int kills;
    private int deaths;

    private int points;

    private int playedRounds;
    private int roundsWon;
    private int chestOpened;

    private String achievements;

    private UUID uuid;

    public User(Player player) {
        if (UserFetcher.playerExists(player.getUniqueId().toString().replaceAll("-", ""))) {
            setUUID(player.getUniqueId());
            BattleRoyal.getInstance().activeUsers.put(getUUID(), this);
            BattleRoyal.getInstance().getMySQLHandler().new LoadUser(getUUID().toString());
        } else {
            setKills(0);
            setDeaths(0);
            setPoints(100);
            setPlayedRounds(0);
            setRoundsWon(0);
            setChestOpened(0);
            setAchievements("-1");
            setUUID(player.getUniqueId());
            UserFetcher.createUser(this);
            BattleRoyal.getInstance().activeUsers.put(getUUID(), this);
            BattleRoyal.getInstance().getMySQLHandler().new LoadUser(getUUID().toString());
        }
    }

    public void update() {
        String statement =
            "UPDATE users SET Kills = ?, Deaths = ?, Points = ?, PlayedRounds = ?, ChestOpened = ?, Achievements = ? WHERE UUID = ?;";
        BattleRoyal.getInstance().getMySQLHandler().new Query(statement, true, getKills(), getDeaths(),
            getPoints(), getPlayedRounds(), getChestOpened(), getAchievements(), getCompactUUID());
    }

    public String getAchievements() {
        return achievements;
    }

    public void setAchievements(String achievements) {
        this.achievements = achievements;
    }

    public String getCompactUUID() {
        return getUUID().toString().replaceAll("-", "");
    }

    public UUID getUUID() {
        return uuid;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public int getPoints() {
        return points;
    }

    public void setUUID(UUID uuid) {
        this.uuid = uuid;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getPlayedRounds() {
        return playedRounds;
    }

    public void setPlayedRounds(int playedRounds) {
        this.playedRounds = playedRounds;
    }

    public int getRoundsWon() {
        return roundsWon;
    }

    public void setRoundsWon(int roundsWon) {
        this.roundsWon = roundsWon;
    }

    public int getChestOpened() {
        return chestOpened;
    }

    public void setChestOpened(int chestOpened) {
        this.chestOpened = chestOpened;
    }
}

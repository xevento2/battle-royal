package net.tyan.battleroyal.scheduler;

import net.tyan.battleroyal.BattleRoyal;
import net.tyan.battleroyal.game.GameState;
import net.tyan.battleroyal.listener.PreparingListener;
import net.tyan.battleroyal.reference.Reference;
import net.tyan.battleroyal.utility.ListenerBundle;
import net.tyan.battleroyal.utility.MapTeleport;
import net.tyan.battleroyal.utility.TabActionTitle;
import org.bukkit.Bukkit;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * by Kevin on 24.07.2015.
 */

public class LobbyScheduler implements Runnable {

    private final BattleRoyal instance;

    public static int time;
    public final int runID;

    public LobbyScheduler(BattleRoyal instance) {
        this.instance = instance;
        time = 241;
        runID = Bukkit.getScheduler().scheduleSyncRepeatingTask(instance, this, 20L, 20L);
    }

    @Override public void run() {
        if (Bukkit.getOnlinePlayers().isEmpty()) {
            time = 241;
            return;
        }

        if (Bukkit.getOnlinePlayers().size() >= 12) {
            if (time > 80)
                time = 80;
        }

        for (Player player : Bukkit.getOnlinePlayers()) {
            TabActionTitle.sendActionBar(player,
                "§3Die Runde startet in §6" + time + (time == 1 ? " Sekunde" : " Sekunden") + "§7.");
        }

        if (time == 240 || time == 210 || time == 180 || time == 150 || time == 120 || time == 90
            || time == 60 || time == 30 || time == 15 || time == 10 || time == 5 || time < 5) {

            if (time < 0)
                return;

            Bukkit.broadcastMessage(
                Reference.PREFIX + "§7Die Runde startet in §6" + time + (time == 1 ?
                    " Sekunde" :
                    " Sekunden") + "§7.");

            for (Player player : Bukkit.getOnlinePlayers()) {
                player.playNote(player.getLocation(), Instrument.PIANO,
                    new Note(0, (time == 1 ? Note.Tone.C : Note.Tone.E), true));
            }

            if (Bukkit.getOnlinePlayers().size() < 2 && time < 60) {
                Bukkit.broadcastMessage(Reference.PREFIX
                    + "§cDer Countdown wurde neu gestartet, da zu wenig Spieler online sind. (mind. 2 Spieler)");

                for (Player player : Bukkit.getOnlinePlayers())
                    player.playSound(player.getLocation(), Sound.ANVIL_BREAK, 5f, 5f);

                time = 241;
            }
        }
        if (time == 0) {

            instance.getServer().getScheduler().cancelTask(runID);
            BattleRoyal.getInstance().setGameState(GameState.PREPARING);

            ListenerBundle.unregister("lobby");

            ListenerBundle preparingListener = new ListenerBundle();
            preparingListener.add(new PreparingListener());
            ListenerBundle.register("preparing", preparingListener);

            new MapTeleport();
            new PreparingScheduler(instance);
        }

        time--;
    }
}

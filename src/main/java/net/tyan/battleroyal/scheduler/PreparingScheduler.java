package net.tyan.battleroyal.scheduler;

import net.tyan.battleroyal.BattleRoyal;
import net.tyan.battleroyal.game.GameState;
import net.tyan.battleroyal.listener.InGameListener;
import net.tyan.battleroyal.listener.PreparingListener;
import net.tyan.battleroyal.reference.Reference;
import net.tyan.battleroyal.utility.ListenerBundle;
import org.bukkit.Bukkit;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.entity.Player;

/**
 * by Kevin on 24.07.2015.
 */

public class PreparingScheduler implements Runnable {

    private final BattleRoyal instance;

    private int time;
    public final int runID;

    public PreparingScheduler(BattleRoyal instance) {
        this.instance = instance;
        this.time = 120;
        this.runID = Bukkit.getScheduler().scheduleSyncRepeatingTask(instance, this, 20L, 20L);
    }

    @Override
    public void run() {

        if (time == 120 || time == 60 || time == 30 || time == 5 || time < 3) {

            if (time < 0)
                return;

            Bukkit.broadcastMessage(
                Reference.PREFIX + "§7Ihr seid ihn §6" + time + (time == 1 ?
                    " Sekunde" :
                    " Sekunden") + "§7 verwundbar.");

            for (Player player : Bukkit.getOnlinePlayers()) {
                player.playNote(player.getLocation(), Instrument.PIANO,
                    new Note(0, (time == 1 ? Note.Tone.C : Note.Tone.E), true));
            }
        }
        if (time == 0) {
            Bukkit.broadcastMessage(Reference.PREFIX + "§7Ihr seid nun verwundbar!");
            instance.getServer().getScheduler().cancelTask(runID);
            BattleRoyal.getInstance().setGameState(GameState.INGAME);
            ListenerBundle.unregister("preparing");
            PreparingListener.ignored.clear();

            ListenerBundle inGameListener = new ListenerBundle();
            inGameListener.add(new InGameListener());
            ListenerBundle.register("inGame", inGameListener);
        }

        time--;
    }
}

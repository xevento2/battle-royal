package net.tyan.battleroyal.utility;

import net.tyan.battleroyal.BattleRoyal;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.util.*;

/**
 * by Kevin on 24.07.2015.
 */

public class ListenerBundle {

    private final List<Listener> listeners;

    private static Map<String, ListenerBundle> bundles;

    static {
        ListenerBundle.bundles = new HashMap<>();
    }

    public static void register(String name, ListenerBundle bundle) {
        ListenerBundle.bundles.put(name, bundle);
        bundle.listeners.forEach(listener -> {
            Bukkit.getPluginManager().registerEvents(listener, BattleRoyal.getInstance());
        });
    }

    public static void unregister(String name) {
        if (ListenerBundle.bundles.containsKey(name)) {
            ListenerBundle.bundles.remove(name).listeners.forEach(HandlerList::unregisterAll);
        }
    }

    public ListenerBundle() {
        this.listeners = new ArrayList<>();
    }

    public void add(Listener listener, Listener... listeners) {
        this.listeners.add(listener);
        this.listeners.addAll(Arrays.asList(listeners));
    }

    public List<Listener> get() {
        return this.listeners;
    }
}
